students = ["Frank","John","Paul","Dave","Mary"]
grades = [90,80,70,60,50]
for i in range(len(students)):
    print(f"The grade of {students[i]} is {grades[i]}")


car = {
    "brand": "Ford",
    "model": "Mustang",
    "year_of_make": 1964,
    "color": "red"
}

print(f"I own a {car['brand']} {car['model']} and it was made in {car['year_of_make']}")

